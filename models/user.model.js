const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  username: {type: String, required: true, min: 6, max: 100},
  email: {type: String, required: true},
  password: {type: String, required: true, max: 1024},
  subdate: {type: Date, default: Date.now},
});


// Export the model
module.exports = mongoose.model('User', UserSchema);
