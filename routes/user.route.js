const router = require('express').Router();
const userController = require('../controllers/user.controller');

router.get('/users', userController.getUsers);
router.get('/users/:id', userController.getSingleUser);
router.put('/users/:id/update', userController.updateUser);
router.put('/users/:id/delete', userController.deleteUser);

module.exports = router;