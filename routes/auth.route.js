const router = require('express').Router();
const User = require('../models/user.model');
const Joi = require('@hapi/joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation, loginValidation} = require('../validations/user.validation')

//Register User
router.post('/register', async (req, res)=> {
    //Validating the data we parse in body
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //Checking if the email is valid
    const existEmail = await User.findOne({email: req.body.email});
    if (existEmail) return res.status(400).send("Email already exist");
    //Hashing the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword =await bcrypt.hash(req.body.password, salt);
    //Creating new user
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        password: hashedPassword
    });
    try{
        const savedUser = await user.save();
        res.send({user: user._id});
    }catch(err){
        res.status(400).send(err)
    }
})

//Login User
router.post('/login', async (req, res)=> {
    //Validating the data
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    //Checking if email is valid
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).send("Wrong Email or Password");
    //Validate password
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send("Wrong Email or Password");
    //Generating Token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_KEY, {expiresIn: "2 days"});
    res.header('access_token', token).send(token);
});
module.exports = router;