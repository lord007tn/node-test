const express = require('express');
const app = express();
const boom = require('@hapi/boom');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');


dotenv.config();
//Connect to database
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(process.env.DB_CONNECT, () => console.log('connected to db'));

//Importe routes

const authRoute = require('./routes/auth.route');
const postRoute = require('./routes/post.route');
const userRoute = require('./routes/user.route');
//Middleware

app.use(express.json());
//Route Middleware
app.use('/api', authRoute);
app.use('/api/post', postRoute);
app.use('/api/user', userRoute);
let port = 8000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});