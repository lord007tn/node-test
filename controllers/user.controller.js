const User = require('../models/user.model');
const boom = require('@hapi/boom');

//Get all users
const getUsers = async (req, res) => {
    try{
        const users = await User.find();
        return res.send(users);
    }catch(err){
        throw res.send(boom.notFound(err))
    }
}

//Retrive one user

const getSingleUser = async (req, res) => {
    try{
        const id = req.params.id;
        const user = await User.findById(id);
        return res.send(user);
    }catch(err){
        throw res.send(boom.boomify(err))
    }
}


const updateUser = async(req, res) => {

    try{
        const id = req.params.id;
        const user = req.body;
        const { ...updateData } = user
        const existEmail = await User.findOne({email: user.email});
        if (existEmail) return res.status(400).send("Email already exist");
        const updateUser = await User.findByIdAndUpdate(id, updateData, {new: true} );
        return res.send(updateUser);
    }catch(err){
        throw res.send(boom.boomify(err))
    }
}
const deleteUser = async (req, reply) => {
    try {
      const id = req.params.id
      const user = await User.findByIdAndRemove(id)
      return res.send(user)
    } catch (err) {
      throw boom.boomify(err)
    }
  }

module.exports.getUsers = getUsers;
module.exports.getSingleUser = getSingleUser;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;